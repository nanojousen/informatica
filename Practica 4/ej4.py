
# CONSTANTES
TOTAL_MASTERES = 3
ALUMNOS_MASTER = 5
TOTAL_ASIGNATURAS = 5

'''
TIPOS
    registro Alumno
        dni, nombre, apellidos : str
        notas[TOTAL_ASIGNATURAS] : float
    fin registro
    
VARIABLES
    datos[TOTAL_MASTERES][ALUMNOS_MASTER] : Alumno
'''

from random import uniform

class Alumno:
    def __init__(self):
        self.dni = ""
        self.nombre = ""
        self.apellidos = ""
        self.notas = [0.0] * TOTAL_ASIGNATURAS

datos = []
for _ in range (TOTAL_MASTERES): 
    datos.append ([Alumno() for _ in range (ALUMNOS_MASTER)])

for i in range (TOTAL_MASTERES):
    print(f"Datos del master {i+1}: ")
    for j in range (ALUMNOS_MASTER):
        print(f"Alumno {j+1}: ")
        datos[i][j].dni = input ("DNI: ")
        datos[i][j].nombre = input("Nombre: ")
        datos[i][j].apellidio = input("Apellidos: ")
        for k in range (TOTAL_ASIGNATURAS):
            '''
            while True:
                try:
                    datos[i][j].notas[k] = float(input(f"Dame la nota {k+1}: "))
                except ValueError:
                    print("ERROR: valor no valido")
                    continue            
                break
            '''
            datos[i][j].notas[k] = uniform(0,10)

for i in range (TOTAL_MASTERES):
    nota_media_max = -1.0
    ind_alumno = -1
    for j in range (ALUMNOS_MASTER):
        nota_media = 0
        for k in range (TOTAL_ASIGNATURAS):
            nota_media += datos[i][j].notas[k]
        nota_media/=TOTAL_ASIGNATURAS
        if (nota_media>nota_media_max):
            nota_media_max = nota_media
            ind_alumno = j
    print(f"{datos[i][ind_alumno].nombre}\
          {datos[i][ind_alumno].apellidos} \
            ha obtenido la nota media mas alta: {nota_media_max:.2f}")