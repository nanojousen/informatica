
# CONSTANTES

MAX_CALLES=5
MAX_VIVIENDAS=10

'''
TIPOS
    registro Calles:
        nombre:str
        num_viviendas=int
        viviendas=Viviendas
    fin registro
    
    registro Viviendas:
        portal,planta:int
        puerta:str
    fin registro
VARIABLES
    cont_calles:int
'''
class Viviendas:
    def __init__(self):
            self.numero=0
            self.planta=0
            self.puerta=""

class Casas:
    def __init__(self):
        self.nombre=""
        self.num_viviendas=0
        self.viviendas=[Viviendas() for _ in range (MAX_VIVIENDAS)]

calles=[Casas() for _ in range (MAX_CALLES)]

cont_calles=0

while True:
    print("\n1. Registrar calle.\n2. Mostrar todos los datos.\n3. Mostrar viviendas de una calle y numero.\n4. Salir del programa.")

    try:
        opc=int(input("\nQue quieres hacer?: "))
    except ValueError:
        print("ERROR: valor no valido")
        continue

    if (opc<1 or opc>4):
        print("ERROR: la opcion debe estar entre 1 y 4")
        continue

    if (opc==1):
        encontrado=-1
        while True:
            nombre_calle=input("Dime el nombre de la calle: ")
            for i in range (cont_calles):
                encontrado=calles[i].nombre.find(nombre_calle)
                if (encontrado!=-1):
                    break
            if (encontrado!=-1):
                aux=cont_calles
                pos_calle=i
                cont_calles=pos_calle
            else:
                if (cont_calles==5):
                    print("Error: no se puede añadir una nueva calle") 
                    break
                calles[cont_calles].nombre=nombre_calle

            while True:
                try:    
                    calles[cont_calles].num_viviendas=int(input("Dime el numero de viviendas que hay: "))
                except ValueError:
                    print("ERROR: valor no valido")
                    continue

                if (calles[cont_calles].num_viviendas>=0 and calles[cont_calles].num_viviendas<=MAX_VIVIENDAS):
                    break

                print("Error: no se pueden añadir más de 10 viviendas")

            for i in range (calles[cont_calles].num_viviendas):
                while True:
                    try:
                        calles[cont_calles].viviendas[i].numero=int(input("Dime el numero de la vivienda: "))
                    except ValueError:
                        print("ERROR: valor no valido")
                        continue
                    break

                while True:
                    try: 
                        calles[cont_calles].viviendas[i].planta=int(input("Dime el numero de la planta: "))
                    except ValueError:
                        print("ERROR: valor no valido")
                        continue
                    break

                calles[cont_calles].viviendas[i].puerta=input("Dime la letra de la puerta: ")

            if (encontrado!=-1):
                cont_calles=aux
                break

            cont_calles+=1
            print("\nCalle registrada correctamente")
            break

    if (opc==2):
        for i in range (cont_calles):
            print(f"\nCalle: {calles[i].nombre} ({calles[i].num_viviendas} viviendas)")
            for j in range (calles[i].num_viviendas):
                print(f"    Vivienda {j+1}: número {calles[i].viviendas[j].numero},{calles[i].viviendas[j].planta}{calles[i].viviendas[j].puerta}")

    if (opc==3):
        if (cont_calles!=0):
            while True:
                nombre_calle=input("Nombre de la calle: ")
                for i in range (cont_calles):
                    encontrado=nombre_calle.find(calles[i].nombre)
                    if (encontrado!=-1):
                        pos_calle=i
                        break
            
                if (encontrado==-1):
                    print("Error: esa calle no existe")
                    continue
                break

            while True:
                try:
                    numero_calle=int(input("Número de calle: "))
                except ValueError:
                    print("ERROR: valor no valido")
                    continue

                no_viviendas=0

                for i in range (calles[pos_calle].num_viviendas):
                    if (numero_calle==calles[pos_calle].viviendas[i].numero):
                        no_viviendas+=1
            
                if (no_viviendas!=0):
                    break

                else:
                    print("Error: no se han encontrado viviendas con ese número de calle")

            for i in range (calles[pos_calle].num_viviendas):
                if (numero_calle==calles[pos_calle].viviendas[i].numero):
                    print(f"Vivienda {calles[pos_calle].viviendas[i].planta}{calles[pos_calle].viviendas[i].puerta}")

        if (cont_calles==0):
            print("ERROR: no hay ninguna calle registrada")

    if (opc==4):
        break