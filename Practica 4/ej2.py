
# CONSTANTES
TOTAL_PRODUCTOS = 5
DIAS_SEMANAS = 7

'''
TIPOS
    registro Producto
        codigo : str
        descrip : str
        stock : int
    fin registro
    
VARIABLES
    productos[TOTAL_PRODUCTOS] : Producto
    ventas[TOTAL_PRODUCTOS][DIAS_SEMANA] : int
    '''

class Producto:
    def __init__(self):
        self.codigo = ""
        self.descrip = ""
        self.stock = 0

productos = [Producto() for _ in range(TOTAL_PRODUCTOS)]
ventas = []

for _ in range(TOTAL_PRODUCTOS):
    ventas.append([0] * DIAS_SEMANAS)

for j in range (DIAS_SEMANAS):
    for i in range (TOTAL_PRODUCTOS):
        while True:
            try:
                ventas[i][j] = int(input(f"Dame la venta del producto {i+1}: "))
            except ValueError:
                print("ERROR: valor no valido")
                continue
            if (ventas[i][j]>=0 and ventas[i][j]<productos[i].stock):
                productos[i].stock -= ventas[i][j]
                break
            print("ERROR: el valor debe ser mayor o igual que 0")