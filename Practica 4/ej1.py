
#CONSTANTES

TOTAL_POBLACIONES = 10
TOTAL_DECADAS = 10

'''
TIPOS
    registro Poblacion
        nombre : str
        censo[TOTAL_DECADAS] : int
    fin registro

VARIABLES
poblaciones[TOTAL_POBLACIONES] : Poblacion
i : int
'''

class Poblacion:
    def __init__(self):
        self.nombre = ""
        self.censo = [0] * TOTAL_DECADAS

poblaciones = [Poblacion() for _ in range (TOTAL_POBLACIONES)]

for i in range (TOTAL_POBLACIONES):
    poblaciones[i].nombre = input(f"Dame el nombre de la poblacion {i+1}: ")
    for j in range (TOTAL_DECADAS):
        while True:
            try:
                poblaciones[i].censo[j] = int(input(f"Dame el censo de la poblacion {j+1}: "))
            except ValueError:
                print("ERROR: valor no valido")
                continue
            if (poblaciones[i].censo[j]>0):
                break
            print("ERROR: el valor debe ser mayor que 0")

for i in range (TOTAL_POBLACIONES):
    max_censo = poblaciones[i].censo[0]
    num_decada = 0
    for j in range(1, TOTAL_DECADAS):
        if (poblaciones[i].censo[j]>max_censo):
            max_censo=poblaciones[i].censo[j]
            num_decada=j
    
    print(f"La decada {num_decada+1} es la de mayor censo de {poblaciones[i].censo[i]}")