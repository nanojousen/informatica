
'''
TIPOS
    registro Taller:
        color, matricula : str
        coches[total_coches], num_puertas : int
    fin registro

VARIABLES
coches[total_coches] : int
'''

while True:
    try:
        total_coches=int(input("Dime el número de coches: "))
    except ValueError:
        print("ERROR: valor no valido")
        continue
    if (total_coches>=0 and total_coches<=20):
        break

class Taller:
    def __init__(self):
        self.num_puertas=0
        self.color=""
        self.matricula=""

coches=[Taller() for _ in range (total_coches)] 

veces=[0] * total_coches

puertas3=0
puertas4=0
puertas5=0

for i in range (total_coches):
    coches[i].matricula=input(f"Dime la matricula del coche {i+1}: ")
    while True:
        coches[i].color=input(f"Dime el color del coche {i+1}: ")
        if (coches[i].color=="negro" or coches[i].color=="blanco" or coches[i].color=="rojo" or coches[i].color=="amarillo" or coches[i].color=="verde" or coches[i].color=="azul"):
            break
        print("ERROR: el color debe ser negro, blanco, rojo, amarillo, verde o azul")
        continue

    while True:
        try:
            coches[i].num_puertas=int(input(f"Dime el numero de puertas del coche {i+1}: "))
        except ValueError:
            print("ERROR: valor no valido")
            continue
        if (coches[i].num_puertas==3):
            puertas3+=1
            break
        if (coches[i].num_puertas==4):
            puertas4+=1
            break
        if (coches[i].num_puertas==5):
            puertas5+=1
            break
        print("ERROR: el numero de puertas no es 3,4 o 5")

for i in range (total_coches):
    for j in range (total_coches-1):
        if (coches[i].matricula<coches[j].matricula):
            aux_matricula=coches[i].matricula
            coches[i].matricula=coches[j].matricula
            coches[j].matricula=aux_matricula

            aux_num_puertas=coches[i].num_puertas
            coches[i].num_puertas=coches[j].num_puertas
            coches[j].num_puertas=aux_num_puertas

for i in range (total_coches):
    print(coches[i].matricula)
    
max=0

for i in range (total_coches):
    for j in range (total_coches):
        encontrado=coches[j].color.find(coches[i].color)
        if (encontrado!=-1):
            veces[i]+=1

    if (veces[i]>max):
        pos_coche=i
        max=veces[i]
    
print(f"El color {coches[pos_coche].color} es el que más se repite con un total de {veces[pos_coche]} coches")

cont=0

print(f"3 puertas: ",end="")

for i in range (total_coches):
    if (coches[i].num_puertas==3):
        if (puertas3-1==cont):
            print(coches[i].matricula)
            break
        print(coches[i].matricula, end=" ")
        cont+=1

cont=0

print(f"4 puertas: ",end="")

for i in range (total_coches):
    if (coches[i].num_puertas==4):
        if (puertas4-1==cont):
            print(coches[i].matricula)
            break
        print(coches[i].matricula, end=" ")
        cont+=1

cont=0

print(f"5 puertas: ",end="")

for i in range (total_coches):
    if (coches[i].num_puertas==5):
        if (puertas5-1==cont):
            print(coches[i].matricula)
            break
        print(coches[i].matricula, end=" ")
        cont+=1