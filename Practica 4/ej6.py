
# CONSTANTES

TOTAL_PRODUCTOS=10

'''
TIPOS
    registro Productos:
        nombre, marca : str
        tamaño, precio : float
        unidades : int
    fin registro

VARIABLES
productos[MAX_PRODUCTOS] : int
opcion : int
'''

class Productos:
    def __init__(self):
        self.nombre=""
        self.marca=""
        self.tamaño=0.0
        self.precio=0.0
        self.unidades=0

productos=[Productos() for _ in range (TOTAL_PRODUCTOS)]

print("1. Alta producto.\n2. Baja producto.\n3. Modifica producto.\n4. Consulta productos.\n5. Salir del programa.\n")

cont=0

while True:
    try: 
        opcion=int(input("¿Que quieres hacer?: "))
    except ValueError:
        print("ERROR: valor no valido")
        continue
    
    if (opcion==1):
        if (cont==9):
            print("ERROR: has alcanzado el maximo de productos")
            break
        productos[cont].nombre=input("Dime el nombre del producto: ")
        productos[cont].marca=input("Dime el marca del producto: ")
        while True:
            try:
                productos[cont].tamaño=float(input("Dime el tamaño del producto (kg): "))
            except ValueError:
                print("ERROR: valor no valido")
                continue
            break
        while True:
            try:
                productos[cont].precio=float(input("Dime el precio del producto: "))
            except ValueError:
                print("ERROR: valor no valido")
                continue
            break
        while True:
            try:
                productos[cont].unidades=int(input("Dime las unidades del producto: "))
            except ValueError:
                print("ERROR: valor no valido")
                continue
            break
        cont+=1

    if (opcion==2):
        if (cont==0):
            print("ERROR: no hay ningun producto dado de alta")
            break
        
        for i in range (cont):
            print(productos[i].nombre)

        elim=input("\nQue producto quieres eliminar: ")

        for i in range (cont):
            encontrado=productos[i].nombre.find(elim)
            pos_elim=i
            if (encontrado!=-1):
                print(pos_elim)
                productos[encontrado].nombre=""
                productos[encontrado].marca=""
                productos[encontrado].tamaño=0.0
                productos[encontrado].precio=0.0
                productos[encontrado].unidades=0
                cont-=1
                break

    for i in range (cont):
        print(productos[i].nombre)

    print(cont)

        
