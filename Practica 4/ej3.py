
# CONSTANTES

'''
TIPOS
    registro Punto
        x : float
        y : float
    fin registro
    
VARIABLES
    triangulo[3] : Punto
'''

class Punto:
    def __init__(self):
        self.x = 0.0
        self.y = 0.0

triangulo = [Punto() for _ in range(3)]

while True:
    try:
        opcion=int(input("1. Leer triangulo.\n2. Mostrar triangulo.\n3. Calcular area.\n4. Calcular perimetro.\n5. Calcular punto medio.\n6. Salir.\n¿Que quieres hacer?: "))
    except ValueError:
        print("ERROR: valor no valido")
        continue
    if (opcion<1 and opcion>6):
        print("ERROR: el numero debe estar entre 1 y 6 (ambos incluidos)")

    if (opcion==1):
        for i in range (3):
            print(f"Vertice {i+1}: ")
            while True:
                    try:
                        triangulo[i].x = float(input(f"Dame la coordenada x: "))
                    except ValueError:
                        print("ERROR: valor no valido")
                        continue
                    break
            while True:
                    try:
                        triangulo[i].y = float(input(f"Dame la coordenada y: "))
                    except ValueError:
                        print("ERROR: valor no valido")
                        continue            
                    break

    if (opcion==2):
        for i in range (3):
            print(f"Las coordenadas del vertice {i+1} son: {(triangulo[i].x,triangulo[i].y)}")

    if (opcion==3):
        v1v2_x = triangulo[1].x - triangulo[0].x
        v1v2_y = triangulo[1].y - triangulo[0].y
        aux = v1v2_y
        v1v2_y = v1v2_x
        v1v2_x = -aux
        v1v3_x = triangulo[2].x - triangulo[0].x
        v1v3_y = triangulo[2].y - triangulo[0].y
        area=((v1v2_x * v1v3_x) + (v1v2_y * v1v3_y))/2
        print(f"El area es: {area}")

    if (opcion==4):
        distancia1=((triangulo[1].x-triangulo[0].x)**2+(triangulo[1].y-triangulo[0].y)**2)**(1/2)
        distancia2=((triangulo[2].x-triangulo[1].x)**2+(triangulo[2].y-triangulo[1].y)**2)**(1/2)
        distancia3=((triangulo[2].x-triangulo[0].x)**2+(triangulo[2].y-triangulo[0].y)**2)**(1/2)
        print(f"El perimetro es {distancia1+distancia2+distancia3:.2f}")

    if (opcion==5):
        print(f"El punto medio es: ({(triangulo[0].x+triangulo[1].x+triangulo[2].x)/3:.2f},{(triangulo[0].y+triangulo[1].y+triangulo[2].y)/3:.2f})")

    if (opcion==6):
        break














