
'''
VARIABLE
num:float
i:int
'''
i=0
while True:
    try:
        i=i+1
        num=float(input(f"{i}: "))
    except ValueError:
        print("ERROR: el valor no es numero")
        i=i-1
        continue
    if (num<0):
        break

print(f"La cantidad de numeros que has puesto es: {i-1}")