'''
VARIABLES
num : int
saldo, ingreso, retiro : float
'''

saldo=1000.0

while True:

    print("Opciones:\n1. Consultar saldo.\n2. Realizar un ingreso.\n3. Realizar un retiro.\n4. Salir.")
      
    while True:
        try:
            num=int(input("Diga el numero de lo que desea hacer: "))
        except ValueError:
            print("ERROR: el valor no es un numero")
            continue
        if (num>=1 and num<=4):
            break
        else:
            print("ERROR: el valor no esta entre 1 y 4")

    if (num==1):
        print(f"Saldo actual: {saldo:.2f}")

    if (num==2):
        while True:
            try:
                ingreso=float(input("¿Cuánto desea ingresar?: "))
            except ValueError:
                print("ERROR: el valor no es un numero")
                continue
            if (ingreso>0):
                saldo+=ingreso
                print("Ingreso realizado correctamente")
                break
            else:
                print("ERROR: el valor no es un numero válido")

    if (num==3):
        while True:
            try:
                retiro=float(input("¿Cuánto desea retirar?: "))
            except ValueError:
                print("ERROR: el valor no es un numero")
                continue
            if (retiro<=saldo and retiro>0):
                saldo-=retiro
                print("Retiro realizado correctamente")
                break
            if (retiro<=0):
                print("ERROR: el valor no es un numero válido")
            if (retiro>saldo):
                print("Saldo insuficiente")
                break

    if (num==4):
        break