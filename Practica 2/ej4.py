
'''
VARIABLES

n1:int
n2:int
n3:int
'''
while True:
    try:
        n1=int(input("Dime el primer numero: "))
    except ValueError:
        print("ERROR: el valor no es un entero")
        continue
    break

while True:
    try:
        n2=int(input("Dime el segundo numero: "))
    except ValueError:
        print("ERROR: el valor no es un entero")
        continue
    break

while True:
    try:
        n3=int(input("Dime el tercer numero: "))
    except ValueError:
        print("ERROR: el valor no es un entero")
        continue
    break

if (n1<=n2 and n1<=n3):
    if (n2<=n3):
        print(f"{n1} <= {n2} <= {n3}")
    else:
        print(f"{n1} <= {n3} <= {n2}")

elif (n2<=n1 and n2<=n3):
    if (n1<=n3):
        print(f"{n2} <= {n1} <= {n3}")
    else:
        print(f"{n2} <= {n3} <= {n1}")

else:
    if (n1<=n2):
        print(f"{n3} <= {n1} <= {n2}")
    else:
        print(f"{n3} <= {n2} <= {n1}")