
# CONSTANTES
'''
VARIABLES
i:int
suma:float
inicio:int
final:int
'''
suma=0

while True:
    try:
        inicio=int(input("Dime el primer valor del sumatorio: "))
    except ValueError:
        print("ERROR: el valor no es un entero")
        continue
    break

while True:
    try:
        final=int(input("Dime el ultimo valor del sumatorio: "))
    except ValueError:
        print("ERROR: el valor no es un entero")
        continue
    if (final>inicio):
        break
    print("ERROR: el valor final es menor que el de inicio")

for i in range (inicio,final+1):
    suma=2*i+1+suma

print(f"El valor del sumatorio es: {suma}")