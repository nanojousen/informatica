
'''
VARIABLES
i, j, p:int
num:int
'''
i=0
j=0
p=0

while True:
    num=int(input(""))
    if (num>=1 and num<=5):
        i+=1
    if (num>=6 and num<=10):
        j+=1
    if (num<=-1 or num>=11):
        p+=1
    if (num==0):
        break

print(f"{i} numeros hay entre 1 y 5")
print(f"{j} numeros hay entre 6 y 10")
print(f"{p} numeros hay fuera de los otros intervalos")