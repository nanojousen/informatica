
'''
VARIABLES
mes:int
'''

while True:
    try:
        mes=int(input("Dime el numero del mes: "))
    except ValueError:
        print("ERROR: el valor no es un numero entero")
        continue
    if (mes>=1 and mes<=12):
        break
    print("El numero no corresponde con ningun mes")

if (mes==2):
    print("El mes tiene 28 dias")

if (mes==1 or mes==3 or mes==5 or mes==7 or mes==8 or mes==10 or mes==12):
    print("El mes tiene 31 dias")

if (mes==4 or mes==6 or mes==9 or mes==11):
    print("El mes tiene 30 dias")