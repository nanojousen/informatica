
'''
VARIABLES
i,n:int
num,max,min:int
'''

suma=0

while True:
    try:
        n=int(input("Dime el numero de valores que se pondran (n>1): "))
    except ValueError:
        print("ERROR: el numero no es un entero")
        continue
    if (n>1):
        break
    print("El numero no es mayor que 1")

print("Dime numeros positivos ")

for i in range (1,n+1):
    while True:
        try:
            num=int(input(""))
        except ValueError:
            print("ERROR: el numero no es un entero")
            continue
        if (num>0):
            break
        print("El numero no es mayor que 0")
    suma+=num
    if (i==1):
        max=num
        min=num
    if (num<min and i!=1):
        min=num
    if (num>max and i!=1):
        max=num
    
print(f"La suma es {suma}\nLa media es {suma/n:.2f}\nEl maximo es {max}\nEl minimo es {min}")