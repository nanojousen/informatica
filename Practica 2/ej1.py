
'''
VARIABLES
tramo:int
tiempo:int
npasos:int
'''

while True:
    try:
        tiempo=int(input("Dime la duracion de la llamada (seg): "))
    except ValueError:
        print("ERROR: el valor no es un numero")
        continue
    if (tiempo>0):
        break
    print("ERROR: el tiempo no es mayor que 0")


while True:
    try:
        tramo=int(input("Dime el tramo en el que se hizo la llamada (1, 2 o 3): "))
    except ValueError:
        print("ERROR: el valor no es un numero")
        continue
    if (tramo>=1 and tramo<=3):
        break
    print("ERROR: el valor no es 1, 2 o 3")

if (tramo==1):
    npasos=tiempo//60
    print(f"El precio de la llamada es {0.1+npasos*0.03:.2f} euros")

if (tramo==2):
    npasos=tiempo//20
    print(f"El precio de la llamada es {0.1+npasos*0.03:.2f} euros")

if (tramo==3):
    npasos=tiempo//40
    print(f"El precio de la llamada es {0.1+npasos*0.03:.2f} euros")