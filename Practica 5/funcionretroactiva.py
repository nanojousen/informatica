
'''
VARIABLES
n:int
'''

def serie(n:int) -> int:
    '''
    variables principales
    
    '''
    # Caso base o trivial
    if (n==0):
        return 0
    if (n==1):
        return 1
    return (serie(n-1))+(serie(n-2))

n=int(input("Dime el numero: "))

print(f"El valor de fibonacci es {serie(n)}")