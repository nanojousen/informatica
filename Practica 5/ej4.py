
def multiplo (n1:int, n2:int) -> int:
    '''
    variables locales
        resto : int
    '''
    resto=n2%n1
    if (resto==0):
        return 1
    else:
        return 0

for i in range (10):
    a=int(input("Dame el primer valor: "))
    b=int(input("Dame el segundo valor: "))
    if (multiplo(a,b)==1):
        print(f"{b} es multiplo de {a}")
    else:
        print(f"{b} no es multiplo de {a}")