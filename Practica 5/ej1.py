
# CONSTANTES

MAX_PALABRAS = 20

# Declaracion de modulos
def palindromo (cad:str) -> bool:
    '''
    variables locales
        cad_izq_dcha, cad_dcha_izq : str
        i : int
    '''
    cad_izq_dcha=""
    cad_dcha_izq=""
    cad=cad.lower()
    for i in range (len(cad)):
        if (cad[i]!=" "):
            cad_izq_dcha+=cad[i]
    
    for i in range (len(cad_izq_dcha)-1,-1,-1):
        cad_dcha_izq+=cad_izq_dcha[i]

    if (cad_dcha_izq==cad_izq_dcha):
        return True
    return False

# Inicio

texto=input("Dame el texto: ")
if (palindromo(texto)):
    print("Es palindromo")
else:
    print("No es palindromo")