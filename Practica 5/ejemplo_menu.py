'''
PROGRAMA ejemplo de menu

CONSTANTES
'''
TOTAL_OPCIONES=5
'''
VARIABLES
opcion : int
'''
# Inicializacion de variables y EEDD


# Declaracion de MODULOS
def menu_opciones () -> int:
    '''
    variables locales
        opc : int
    '''
    for i in range (TOTAL_OPCIONES):
        print(f"{i+1}. Opcion {i+1}")
    while True:
        try:
            opc=int(input("Opcion: "))
        except ValueError:
            print("ERROR: valor no valido")
            continue
        if opc<1 or opc>TOTAL_OPCIONES:
            print(f"ERROR: la opcion debe estar entre 1 y {TOTAL_OPCIONES}")
            continue
        return opc
    
# Inicio
    
opcion=menu_opciones()

print(f"La opcion elegida es {opcion}")