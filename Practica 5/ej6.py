
def mcd (a:int, b:int):
    '''
    variables principales
        aux,resto_a, resto_b,i:int
    '''
    if (a<b):
        aux=a
        a=b
        b=aux

    for i in range (b):
        resto_a=a%(b-i)
        resto_b=b%(b-i)
        if (resto_a==0 and resto_b==0):
            print(f"El maximo comun divisor es {b-i}")
            break

def mcm (a:int, b:int):
    '''
    variables principales
        aux,resto_a,resto_b,i:int
    '''
    if (a<b):
        aux=a
        a=b
        b=aux

    for i in range (a,a*b+1):
        resto_a=i%a
        resto_b=i%b
        if (resto_a==0 and resto_b==0):
            print(f"El minimo comun multiplo es {i}")
            break

n1=int(input("Dame el primer numero: "))
n2=int(input("Dame el segundo numero: "))

mcd(n1,n2)
mcm(n1,n2)