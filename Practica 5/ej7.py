'''
Programa Taller de chapa y pintura (versión modular)

CONSTANTES
'''
MAX_COCHES = 20
'''
TIPOS 
    registro Coche
        matricula, color : str
        puertas : int
    fin_registro

VARIABLES
    coches[MAX_COCHES] : Coche
    tam_log, opc, num_puertas : int
'''
#----------------------------------------------------------------        
# inicialización de Tipos
class Coche:
    def __init__(self):
        self.matricula = ""
        self.color = ""
        self.puertas = 0
#----------------------------------------------------------------        
# inicialización de variables y EEDD
coches = [ Coche() for _ in range(MAX_COCHES) ]
tam_log = 0

#----------------------------------------------------------------        
# definición de MÓDULOS
def menu_opciones () -> int:    
    '''
    variables locales        
        opc : int
    '''
    print("\nMENÚ DE OPCIONES:")
    print("1. Nuevo coche")
    print("2. Listado coches")
    print("3. Color más repetido")
    print("4. Coches con x puertas")
    print("5. Salir")
    while True:
        try:
            opc = int(input("Opción: "))
        except ValueError:
            print("Error: se esperaba un entero")
            continue
        if (opc >= 1 and opc <= 5):
            return opc
        print("Error: el número de opción no es válido")

# AÑADIR AQUÍ LOS MÓDULOS NECESARIOS
def nuevo_coche (tam_log):
    coches[tam_log].matricula=input(f"Dime la matricula del coche {tam_log+1}: ")
    coches[tam_log].color=input(f"Dime el color del coche {tam_log+1}: ")

    while True:
        try:
            coches[tam_log].puertas=int(input(f"Dime el numero de puertas del coche {tam_log+1}: "))
        except ValueError:
            print("ERROR: valor no valido")
            continue
        if (coches[tam_log].puertas>=3):
            break
        print("ERROR: el numero de puertas no es 3,4 o 5")

def listado_coches(tam_log):
    for i in range (tam_log):
        for j in range (tam_log-1):
            if (coches[i].matricula<coches[j].matricula):
                aux_matricula=coches[i].matricula
                coches[i].matricula=coches[j].matricula
                coches[j].matricula=aux_matricula

    for i in range (tam_log):
        print(coches[i].matricula)
        
def color_mas_repetido(tam_log) -> str:
    max=0
    veces=[0] * tam_log

    for i in range (tam_log):
        for j in range (tam_log):
            encontrado=coches[j].color.find(coches[i].color)
            if (encontrado!=-1):
                veces[i]+=1

        if (veces[i]>max):
            pos_color=i
            max=veces[i]
    return coches[pos_color].color

def coches_con_x_puertas(num_puertas,tam_log) -> int:
    cantidad=0
    for i in range (tam_log):
        if (num_puertas==coches[i].puertas):
            cantidad+=1

    return cantidad
            
#----------------------------------------------------------------        
# INICIO
while True:
    opc = menu_opciones()
    
    if (opc==1): # Nuevo coche
        if (tam_log<MAX_COCHES):
            nuevo_coche(tam_log)
            tam_log+=1
        else:
            print("Error: no se pueden añadir más coches")

    if (opc==2): # Listado de coches
        listado_coches(tam_log)
        
    if (opc==3): # Color más repetido
        print(f"El color más repetido es el {color_mas_repetido(tam_log)}")
    
    if (opc==4):# Coches con x puertas
        while True:
            try:
                num_puertas = int(input("Número de puertas: "))
            except ValueError:
                print("Error: se esperaba un entero")
                continue
            if (num_puertas>=3):
                break
            print(f"Error: número de puertas debe ser >=3")
        
        print(f"Existen un total de {coches_con_x_puertas(num_puertas,tam_log)} coches con {num_puertas} puertas")
        
    if (opc==5): # Salir del programa
        break

# FIN