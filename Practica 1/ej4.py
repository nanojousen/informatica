
DOLAR = 1.286
LIBRA = 0.865

euros : float
dolares: float
libras: float

try:
    euros=float(input("DIme el numeros de euros: "))
except ValueError:
    print("ERROR: No me estas dando una cantidad de euros")
    exit()

dolares=euros*DOLAR
libras=euros*LIBRA

print(f"EL valor en dolares es: {dolares:.2f}\n y en libras esterlinas es: {libras:.2f}")