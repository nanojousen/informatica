
seg: int
min: int

try:
    seg=int(input("Dime el numero de segundos: "))
except ValueError:
    print("ERROR: Valor no válido")
    exit()

min=seg//60
seg=(seg/60-min)*60

print(f"Son {min} minutos y {seg:.0f} segundos")