
far: float
grados: float

try: 
    far=float(input("Dime el valor en Fahrenheit: "))
except ValueError:
    print("ERROR: Valor no válido")
    exit()

grados=(far-32)*5/9
        
print(f"El valor en grados es: {grados:.1f}")