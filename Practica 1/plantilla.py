#PROGRAMA plantilla



#CONSTANTES

SALUDO = 'Hola'
MENSAJE = 'Por favor, introduza el año en que nació'
UMBRAL = 21

#VARIABLES

fecha:int    #(int=integer)  'float=real'
edad:int

#INICIO

print(f"{SALUDO}")   #se utiliza llave para declarar la variable y constantes en una frase
print(f"{MENSAJE}")

print(f'{SALUDO}\n{MENSAJE}')     #(\n  cambio de linea(punto y aparte))

try:   #(control de errores)
    base = int(input("Dame base: "))

except ValueError:
    print("ERROR: tipo de base no permitido")
    exit()

print(f"{edad > UMBRAL}") 

#(para que una division salga entera necesitamos por dos barras en vez de una)
#j: 1=3//2

#(la f sirve para formatear, cuando utilice variables y constantes o mas de una frase) 
#(utilizar la f siempre en caso de duda)
#(en todos los try y except es obligatorio la tabulacion)
# ValueError poner despues de except para catalogar el error

#esto sirve para declarar pi o otro numero periodico
import math

print(f"{math.pi}") 
#para el numero 'e' es lo mismo solo hay que cambiar 'pi' por 'e'

#ATAJOS

#(ctrl + k + c, me anula todas las lineas, pone almohadillas en todas las seleccionadas)(comenta)
#(ctrl + k + u,lo contrario, la descomenta)
#(ctrl + y , rehacer lo deshecho)
#(ctrl + f, busqueda de palabra y reemplazar(ctrl + h))


#FIN