
import math

radio : float

try:
    radio=float(input("Dime el valor del radio: "))

except ValueError:
    print("ERROR: No es un valor real")
    exit()

print(f"El perimetro es: {2*math.pi*radio}")