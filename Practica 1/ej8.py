
UMBRAL = 21

edad : int

try:
    edad = int(input("Dame la edad en años: "))
except ValueError:
    print("ERROR: tipo no válido")
    exit()

print(f"{edad > UMBRAL}")