

base : int
altura : int

try:
    base = int(input("Dame base: "))

except ValueError:
    print("ERROR: tipo de base no permitido")
    exit()

try:
    altura = int(input("Dame altura: "))

except ValueError:
    print("ERROR: tipo de altura no permitido")
    exit()

area = (base * altura) / 2


print(f"{area:.2f}")