
'''
VARIABLES
m[3][3] : int
i : int
'''

m = []

for _ in range (3):
    m.append([-1] * 3)

sumafil = [0] * 3
sumacol = [0] * 3
for i in range (3):
    for j in range (3):
        while True:
            try:
                m[i][j]=int(input(f"Dime el valor de la coordenada {i+1,j+1}: "))
            except ValueError:
                print("ERROR: valor no valido")
                continue
            break

for i in range (3):
    for j in range (3):
        sumafil[i]+=m[i][j]
        sumacol[i]+=m[j][i]

for i in range (3):
    print(f"La suma de la fila {i+1} es: {sumafil[i]}")

for i in range (3):
    print(f"La suma de la columna {i+1} es: {sumacol[i]}")