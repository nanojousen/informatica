
'''
VARIABLES

cad_original, subcad, nueva_subcad, cad_result:str
'''

cad_result=""

cad_original=input("Cadena original: ")

subcad=input("Subcadena a reemplazar: ")

nueva_subcad=input("Nueva subcadena: ")

cad_original=cad_original.lower()
subcad=subcad.lower()

i=0

while (i<len(cad_original)):
    if (cad_original[i]!=subcad[0]):
        cad_result += cad_original[i]
    else:
        j=1
        while (j<len(subcad) and subcad[j]==cad_original[i+j] and i+j<len(cad_original)):
            j+=1
        if (j==len(subcad)):
            cad_result+=nueva_subcad
            i+=len(subcad)-1
        else:
            cad_result+=cad_original[i]
    i+=1

print(f"La cadena nueva es: {cad_result}")