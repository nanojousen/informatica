
'''
VARIABLES

asignatura[num] : int
horas[4] : int
num : int
'''

mes = [0] * 4
mes[0] = "Febrero"
mes[1] = "Marzo"
mes[2] = "Abril"
mes[3] = "Mayo"

while True:
    try:
        num=int(input("Dime el numero de asignaturas (Max. 10): "))
    except ValueError:
        print("ERROR: valor no valido")
        continue
    if (num>=1 and num<=10):
        break

maxpos=0
asignatura = [0] * num
hasig = [0] * num
hmes = [0] * 4
horas = []
for _ in range (num):
    horas.append([0] * 4)

for i in range (num):
    asignatura[i]=input("Dime el nombre de la asignatura: ")

for i in range (num):
        print(f"Horas dedicadas para {asignatura[i]}")
        for j in range (4):
            while True:
                try:
                    horas[i][j]=float(input(f"En {mes[j]}: "))
                except ValueError:
                    print("ERROR: valor no valido")
                    continue
                break
            hasig[i]+=horas[i][j]
        if (i==0):
            max=hasig[i]
        if (hasig[i]>max and i!=0):
            max=hasig[i]
            maxpos=i

for i in range (num):
    for j in range (4):
        hmes[i]+=horas[j][i]

print("Total de horas dedicadas a cada asignatura:")
for i in range (num):
    print(f"{asignatura[i]}: {hasig[i]} horas")

print("Total de horas dedicadas cada mes:")
for i in range (4):
    print(f"{mes[i]}: {hmes[i]} horas")

print(f"La asignatura con mas horas dedicadas es {asignatura[maxpos]} con {hasig[maxpos]} horas totales")