
'''
VARIABLES
m[i][j] : int
filas, columnas, i, j, suma : int
'''

m1 = []
m2 = []
mproducto = []

while True:
    try:
        filas1=int(input("Dime el numero de filas de la primera matriz: "))
    except ValueError:
        print("ERROR: el valor no es un numero entero")
        continue
    if (filas1>=2 and filas1<=10):
        break

while True:
    try:
        columnas1=int(input("Dime el numero de columnas de la primera matriz: "))
    except ValueError:
        print("ERROR: el valor no es un numero entero")
        continue
    if (columnas1>=2 and columnas1<=10):
        break

while True:
    try:
        filas2=int(input("Dime el numero de filas de la segunda matriz: "))
    except ValueError:
        print("ERROR: el valor no es un numero entero")
        continue
    if (filas2==columnas1):
        break
    else:
        print("ERROR: el numero de filas no coincide con el numero de columnas de la primera matriz")
        continue

while True:
    try:
        columnas2=int(input("Dime el numero de columnas de la segunda matriz: "))
    except ValueError:
        print("ERROR: el valor no es un numero entero")
        continue
    if (columnas2>=2 and columnas2<=10):
        break

for _ in range (filas1):
    m1.append([-1] * columnas1)

for _ in range (filas2):
    m2.append([-1] * columnas2)

for _ in range (filas1):
    mproducto.append([-1] * columnas2)

print("Coordenadas de la primera matriz")

for i in range (filas1):
    for j in range (columnas1):
        m1[i][j]=int(input(f"Dime el valor de la coordenada {i+1,j+1}: "))

print(m1)

print("Coordenadas de la segunda matriz")

for i in range (filas2):
    for j in range (columnas2):
        m2[i][j]=int(input(f"Dime el valor de la coordenada {i+1,j+1}: "))   

print(m2) 

for i in range (filas1):
    for k in range (columnas2):
        suma=0
        for j in range (columnas1):
            suma+=m1[i][j]*m2[j][k]
        mproducto[i][k]=suma
    
print(f"La matriz producto es:\n{mproducto}")
