
'''
VARIABLES

n,i:int
v[n]:float
'''

from math import sqrt

while True:
    try:
        n=int(input("Dime la dimensión del vector (2 <= n <= 10): "))
    except ValueError:
        print("ERROR: el valor no es valido")
    if (n>=2 and n<=10):
        break

v= [-1.0] * n
suma=0.0

for i in range (n):
    while True:
        try:
            v[i]=float(input(f"Dime el numero de la coordenada {i+1}: "))
        except ValueError:
            print("ERROR: valor no valido")
            continue
        break
    suma+=(v[i])**2

print(f"El modulo del vector es {sqrt(suma):.2f}")