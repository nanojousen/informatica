
'''
VARIABLES
m[4][4] : float
v : float
'''

m = []

for _ in range (4):
    m.append([-1] * 4)

for i in range (4):
    for j in range (4):
        while True:
            try:
                m[i][j]=float(input(f"Dime el valor de la coordenada {i+1,j+1}: "))
            except ValueError:
                print("ERROR: valor no valido")
                continue
            if (m[i][j]!=0):
                break

while True:
    try:
        v=float(input("Dime el valor que dividira la matriz: "))
    except ValueError:
        print("ERROR: valor no valido")
        continue
    if (v!=0):
        break

for i in range (4):
    for j in range (4):
        m[i][j]=m[i][j]/v
        print(f"m[{i+1}][{j+1}] = {m[i][j]:.1f}")


