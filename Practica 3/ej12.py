
'''
VARIABLES
m, n, i, j : int
matriz[m][n] : float
'''

matriz = []
matriztraspuesta = []

while True: 
    try: 
        m=int(input("Dime el numero de filas: "))
    except ValueError:
        print("ERROR: valor no valido")
        continue
    if (m>2 and m<8):
        break
    else:
        print("ERROR: valor no comprendido entre 2 y 8")
        continue

while True: 
    try: 
        n=int(input("Dime el numero de columnas: "))
    except ValueError:
        print("ERROR: valor no valido")
        continue
    if (n>2 and n<8):
        break
    else:
        print("ERROR: valor no comprendido entre 2 y 8")
        continue

for _ in range (m):
    matriz.append([-1] * n)

for _ in range (n):
    matriztraspuesta.append([-1] * m)

for i in range (m):
    for j in range (n):
        while True:
            try:
                matriz[i][j]=float(input(f"Dime el valor de la coordenada {i+1,j+1}: "))
            except ValueError:
                print("ERROR: valor no valido")
                continue
            break

print("Original:")

for i in range (m):       
    for j in range (n):
        print(f"{matriz[i][j]:.2f}",end="   ")
    print(end="\n")

        

for i in range (n):
    for j in range (m):
        matriztraspuesta[i][j]=matriz[j][i]

print("Transpuesta:")

for i in range (n):      
    for j in range (m):
        print(f"{matriztraspuesta[i][j]:.2f}",end="   ")
    print(end="\n")
