
'''
VARIABLES
m[i][j] : int
filas, columnas, i, j, suma : int
'''

m1 = []
m2 = []
msuma = []

while True:
    try:
        filas=int(input("Dime el numero de filas de ambas matrices: "))
    except ValueError:
        print("ERROR: el valor no es un numero entero")
        continue
    if (filas>=2 and filas<=10):
        break

while True:
    try:
        columnas=int(input("Dime el numero de columnas de ambas matrices: "))
    except ValueError:
        print("ERROR: el valor no es un numero entero")
        continue
    if (columnas>=2 and columnas<=10):
        break

for _ in range (filas):
    m1.append([-1.0] * columnas)

for _ in range (filas):
    m2.append([-1.0] * columnas)

for _ in range (filas):
    msuma.append([-1.0] * columnas)

print("Coordenadas de la primera matriz")

for i in range (filas):
    for j in range (columnas):
        m1[i][j]=float(input(f"Dime el valor de la coordenada {i+1,j+1}: "))

print("Coordenadas de la segunda matriz")

for i in range (filas):
    for j in range (columnas):
        m2[i][j]=float(input(f"Dime el valor de la coordenada {i+1,j+1}: "))  

for i in range (filas):
    for j in range (columnas):
        msuma[i][j]=m1[i][j]+m2[i][j]

print(msuma)