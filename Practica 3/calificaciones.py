
# CONSTANTES

MIN=10.1

'''
VARIABLES

estudiantes : int
nombres[3<=estudiantes<=30] : str
notas[estudiantes:asignatura], calificacion: float
num,posmin,posnombre,i,j : int
'''

suma=0

while True:
    try: 
        estudiantes=int(input("Dime el numero de estudiantes: "))
    except ValueError:
        print("ERROR: valor no valido")
        continue
    if (estudiantes>=3 and estudiantes<=30):
        break
    else:
        print("El valor no esta entre 3 y 30")
        continue

nombres = ["hola"] * estudiantes

print("Dime los nombres de los estudiantes")

for i in range (estudiantes):
    nombres[i]=input()

notas=[]

for _ in range(estudiantes):
    notas.append([0.0] * 4)

while True:
    try:
        num=int(input("1. Asignar calificación a un estudiante en una asignatura.\n2. Mostrar la calificación más baja de un estudiante y la asignatura.\n3. Estudiante con la calificación más alta en una asignatura.\n4. Calcular la calificación media de un estudiante.\n5. Salir del programa.\n¿Que quieres hacer?: "))
    except ValueError:
        print("ERROR: valor no valido")
        continue

    if (num==1):
        while True:
            nombre=input("¿Nombre del alumno? ")
            for i in range (estudiantes):
                encontrado=nombres[i].find(nombre)
                if (encontrado!=-1):
                    posnombre=i
                    break
                else: 
                    continue
            if (encontrado!=-1):
                break
            else:
                print("ERROR: nombre no encontrado")

        while True:
            try: 
                asignatura=int(input("Dime el valor de la asignatura: "))
            except ValueError:
                print("ERROR: valor no valido")
                continue
            if (asignatura>=1 and asignatura<=4):
                break
        
        while True:
            try: 
                calificacion=float(input("Dime el valor de la calificacion: "))
            except ValueError:
                print("ERROR: valor no valido")
                continue
            if (calificacion>=0 and calificacion<=10):
                break
            
        notas[posnombre][asignatura-1]=calificacion
        
        print("Calificación asignada correctamente\n")

    if (num==2):
        while True:
            nombre=input("¿Nombre del alumno? ")
            for i in range (estudiantes):
                encontrado=nombres[i].find(nombre)
                if (encontrado!=-1):
                    posnombre=i
                    break
                else: 
                    continue
            if (encontrado!=-1):
                break
            else:
                print("ERROR: nombre no encontrado")

        MIN=10.1

        for j in range (4):
            if (notas[posnombre][j]<MIN):
                MIN=notas[posnombre][j]
                posmin=j

        print(f"Calificación más baja de {nombres[posnombre]}: {MIN:.2f} (asignatura {posmin+1})\n")

    if (num==3):
        max=notas[0][0]
        while True:
            try: 
                asignatura=int(input("¿Número de asignatura? "))
            except ValueError:
                print("ERROR: valor no valido")
                continue
            if (asignatura>=1 and asignatura<=4):
                break
        for i in range (estudiantes):
            if (notas[i][asignatura-1]>max):
                max=notas[i][asignatura-1]
                posnombre=i

        print(f"Calificación más alta en la asignatura {asignatura}: {max:.2f} (estudiante {nombres[posnombre]})\n")

    if (num==4):
        suma=0
        while True:
            nombre=input("¿Nombre del alumno? ")
            for i in range (estudiantes):
                encontrado=nombres[i].find(nombre)
                if (encontrado!=-1):
                    posnombre=i
                    break
                else: 
                    continue
            if (encontrado!=-1):
                break
            else:
                print("ERROR: nombre no encontrado")

        for j in range (4):
            if (notas[posnombre][j]!=-1):
                suma+=notas[posnombre][j]
        
        print(f"La calificación media de {nombres[posnombre]} es {suma/4:.2f}\n")

    if (num==5):
        break