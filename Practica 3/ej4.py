
'''
VARIABLES

v[15]:int
'''

v = [-1] * 15

for i in range(15):
    while True:
        try:
            v[i]=int(input(f"Dime el elemento {i+1}: "))
        except ValueError:
            print("ERROR: valor no valido")
            continue
        if (v[i]>=0):
            break
        print("El numero no es positivo")

print("Vector principal")
for i in range(15):
    print(v[i])

print("Vector ordenado decrecientemente")
for i in range(15):
    for j in range(i+1,15):
        if (v[i]<v[j]):
            mayor=v[j]
            v[j]=v[i]
            v[i]=mayor

for i in range(15):
    print(v[i])