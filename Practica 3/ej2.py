
# CONSTANTES
DIM=10


'''
VARIABLES

v1[DIM], v2[DIM], v3[DIM] : int
i : int
'''

# Inicializción de variables

v1=[-1] * DIM
v2=[-1] * DIM
v3=[-1] * DIM

# Inicio
print("Dime los valores de v1")
for i in range(DIM):
    while True:
        try:
            v1[i]=int(input(f"Dime un numero v1({i+1}): "))
        except ValueError:
            print("ERROR: valor no valido")
            continue
        break

    while True:
        try:
            v2[i]=int(input(f"Dime un numero v2({i+1}): "))
        except ValueError:
            print("ERROR: valor no valido")
            continue
        break
    if (v1[i]<=v2[i]):
        v3[i]=v1[i]
    else:
        v3[i]=v2[i]

print(f"Vector 1:", end=" ")
for i in range (DIM):
    print(f"{v1[i]}", end=", ")

print(f"\nVector 2:", end=" ")
for i in range (DIM):
    print(f"{v2[i]}", end=", ")

print(f"\nVector 3:", end=" ")
for i in range (DIM):
    print(f"{v3[i]}", end=", ")
