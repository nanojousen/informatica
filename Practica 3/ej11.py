
'''
VARIABLES
m, n, i, j : int
matriz[i][j] : int
suma : float
'''

suma=0
matriz = []

while True: 
    try: 
        m=int(input("Dime el numero de filas: "))
    except ValueError:
        print("ERROR: valor no valido")
        continue
    if (m>1 and m<9):
        break

while True: 
    try: 
        n=int(input("Dime el numero de columnas: "))
    except ValueError:
        print("ERROR: valor no valido")
        continue
    if (n>1 and n<9):
        break

for _ in range (m):
    matriz.append([-1] * n)

for i in range (m):
    for j in range (n):
        while True:
            try:
                matriz[i][j]=int(input(f"Dime el valor de la coordenada {i+1,j+1}: "))
            except ValueError:
                print("ERROR: valor no valido")
                continue
            break
        if (i==0 and j==0):
            max=matriz[0][0]
            min=matriz[0][0]
        if (matriz[i][j]>max):
            max=matriz[i][j]
        if (matriz[i][j]<min):
            min=matriz[i][j]
        suma+=matriz[i][j]



print(f"El maximo es: {max}")
print(f"El minimo es: {min}")
print(f"La media es: {suma/(m*n):.2f}")
