
'''
VARIABLES

vector_pares[n]:int
vector_impares[m]:int

'''
n=0
m=0
vector_pares= [-1] * 14
vector_impares= [-1] * 14

for i in range(14):
    while True:
        try:
            num=int(input(f"Dime el elemento {i+1}: "))
        except ValueError:
            print("ERROR: valor no valido")
            continue
        break
    cociente=num//2
    resto=num/2-cociente


    if (resto==0):
        vector_pares[n]=num
        n+=1
    else:
        vector_impares[m]=num
        m+=1

print("Vector de numeros pares")
for i in range(n):
    print(vector_pares[i])

print("Vector de numeros impares")
for i in range(m):
    print(vector_impares[i])